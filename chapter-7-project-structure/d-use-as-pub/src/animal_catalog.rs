#[path="animal.rs"]
mod animal;

pub use animal::*;

use Animal as Animaux;

pub fn make_dog() -> Animaux {
    Animal::new("Dog", "woof")
}

pub fn make_cat() -> Animaux {
    Animal::new("Cat", "neko")
}

pub fn make_human() -> Animaux {
    Animal::new("Human", "I have to do my taxes")
}