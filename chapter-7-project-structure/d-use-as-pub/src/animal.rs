
pub struct Animal {
    name : String,
    sound : String,
}

impl Animal {
    pub fn new(name : &str, sound : &str) -> Animal {
        Animal {
            name : String::from(name),
            sound : String::from(sound),
        }
    }

    pub fn make_noise(&self) {
        println!("{}", &self.sound);
    }
    
    pub fn what_am_i(&self) {
        println!("{}", &self.name);
    }
}