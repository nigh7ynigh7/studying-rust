mod animal_catalog;

fn main() {
    let human = animal_catalog::make_human();
    human.what_am_i();
    human.make_noise();
}