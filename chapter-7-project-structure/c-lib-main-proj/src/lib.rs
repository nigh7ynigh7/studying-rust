pub mod i_mod {
    static mut I : i32 = 0;

    pub fn ping() -> i32 {
        unsafe {
            I += 1;
            I
        }
        
    }
}


#[cfg(test)]
mod test_i_mod{

    #[test]
    fn ping_is_1_after_first_call() {
        let a = super::i_mod::ping();
        assert_eq!(a, 1);
    }

}