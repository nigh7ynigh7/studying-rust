mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
        fn sear_at_table() {}
    }
    mod serving {
        fn take_order() {}
        fn server_order() {}
        fn take_payment() {}
    }
}


#[cfg(test)]
mod tests {
    
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn add_to_waitlist_calls() {
        super::front_of_house::hosting::add_to_waitlist()
    }
}
