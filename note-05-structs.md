# structs

## takeaways

- used to represent data
- used to make complex data
- either all of it is mutable or it's not
- similar to js, you can do this with the data:

```rs
struct A {
    a : i32,
    b : i32
}

{
    let a = A {
        a : 10,
        b : 100,
    }
    
    let b = A {
        ...a
    }
}

{
    let a = 10;
    let b = A {
        a,
        b : 100
    }
}

```

- can be borrowed
- if you want to fully move it, implement the `Copy` trait
- structs can have methods associated with them

```rs
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    // akin to an object level method
    fn area(&self) -> u32 {
        self.width * self.height
    }
    
    // akin to an object level method
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    // akin to a static method from java 
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    // ex of usage
    let rect1 Rectangle::square(10);
    let rect2 Rectangle::square(5);
    rect1.can_hold(&rect2);
}
```

- as for their functionality
    - for methods that deal with the object directly
        - you need a `&self` in there
        - apparently, self can be anything (za, warudo, but self is the standard)
        - `.` is the accessor
    - for other methods
        - dont put a `&self` in there
        - `StructName::` is the accessor

- you can have multiple impl blocks for the same struct


## extras

- if you want to easily print something, without having to add additional implementations
    - either implement the Debug trait or...
    - use the `#[derive(Debug)]` annotation

```rs
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}
```

in a print macro....

- `{:?}` will print the data
- `{:#?}` will print the formatted data

---

[previous](./note-04-ownership-and-moving.md) |
[next](./note-06-enums.md)
