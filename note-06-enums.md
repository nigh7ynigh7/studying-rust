# enums and match

## enums

It's what you'd expect:

- it's a way of representing data
- you can use it for states
- you can use it to describe information

```rs
enum Countries {
    Argentina, Australia, Brazil, Canada,
    China, France, Germany, India, Mexico,
    NewZealand, Russia, SouthAfrica, Tunisia,
    UnitedKingdom, UnitedStates
    // ....
}
```

But with rust, they take a step further...

To start things off, they can also take different format

```rs
enum IpAddress {
    IpV4 (u8, u8, u8, u8),
    IpV6 (String)
}
```

You can even put structs in there

```rs
enum GameState {
    Player {
        position : { x : i32, y : i32}
    },
    GameState (GameMode)
}
```

How to use them

- you need to use the `::` operator. example:

```rs
// using the above ip address/*  */
let local = IpAddress::IpV4(127, 0, 0, 1);
```

- What ist covered by the book, you can actually avoid the operator by doing this in whichever block you're coding in:

```rs
use IpAddress::*;
```

- But whether or not you do this, is up to you and your requirements.

### Option<T>

It's an enumerator, too.

It's how rust deals with possible null values.

It has the following values:

- `Option::Some(T)` to state that there is a value
- `Option::None` to say there there is no value

The reason why this exists is to sidestep the existence of nullpointers and to keep
the language as safe as possible. Moreover, this makes the code more imperative and,
in the long run, easier to work with.

## match

in simple terms, `match` is a lot like `switch`, but with several differences:

- they must treat all of the possibilities (they are exhaustive)
- they can work as an expression (`let a = match b { ... }`)
- their default/wildcard operator is: `_`

example usage:

```rs
#[derive(Debug)]
enum CanadianCoins {
    Penny,
    Nickel,
    Dime,
    Quarter,
    Loonie,
    Toonie,
}

fn calculate_value(coin : CanadianCoins) -> f64 {
    match coin {
        CanadianCoins::Penny => 0.01,
        CanadianCoins::Nickel => 0.05,
        CanadianCoins::Dime => 0.1,
        CanadianCoins::Quarter => 0.25,
        CanadianCoins::Loonie => 1,
        CanadianCoins::Toonie => 2,
    }
}

let coin = CanadianCoins::Loonie;

println!("The coin {} has a value of {} canadian dollars", coin, calculate_value(coin));
```

Observe how the previous example cycled through all of the possibilities;

When you want a default value, observe

```rs
enum Alphabet {
    A, B, C, D, /* up until */ Z
}

fn is_abc(term : Alphabet) -> bool {
    use Alphabet::*;
    match term {
        A | B | C => true,
        _ => false,
    }
}

println!("is b in abc? {}", is_abc(Alphabet::B));
```

As you can see this code can build, and `_` is configured as the `default` clause;

### if let

basically, it's and `if` mixed with `match` to allow compare only one value

a simple if:

```rs
if /*condition*/ { }
```

a simple let if

```rs
let x = Some(4);
if let Some(4) = x {
    // code here
}
```

Here are the equivalencies, when you have this:

```rs
let x = Some(4);
match x {
    Some(4) => 4,
    _ => 0,
}
```

the below is equivalent to the above

```rs
let x = Some(4);
if let Some(4) = x {
    4
} else {
    0
}
```

## extra

- the tuple `()` is called a unit
    - you can have an enum with this data type

- you can easily make things printable by adding the `#[drive(Debug)]` annotation. example:

```rs
#[derive(Debug)]
enum State {
    On, Off, Standby, LowPower
}
// ...
println!("{:?}", State::On);
```

-----

[previous](./note-05-structs.md)
