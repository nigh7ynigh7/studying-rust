
# Takeaways so far

## Key takeaways

- `cargo` is similar to `mvn` or `npm` in its building/dependency management capabilities
- `Cargo.toml` is similar to `pom.xml` or `package.json` for dependency management and project meta data.
- `Cargo.lock` is similar to `package-lock.json` for fine tuning exactly which dependency to download.
- `rustc` is basically `javac`

## Commands

    $ cargo doc --open

*what it does*: downloads the documentation for the input libs and opens it on your browser so you can do some studying

    $ rustc main.rs

*what it does*: compiles `main.rs` file

    $ cargo update

*what it does*: checks for library updates, and updates them

    $ cargo build

*what it does*: makes a build of the current project and downloads all of the libs

    $ cargo build --release

*what it does*: generates a release build of the project

## Language

### `match`

Feels like a `switch(){ case; default }` construct.
And it looks like it can be used inline, which is interesting, at the very least.

Usage:

```rust
let guess : u32 = match guess.trim().parse() {
    Ok(num) => num,
    Err(_) => continue,
};
```

### `Result<data, error>`

Result bundles the data and the error together, is how i understood this object.
Used to handle errors.

It does whatever is inside the `.execpt()` and exits the system.

### imports...

You use the keyword `use`.

And there's various ways of using imports

```rust
use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).except("Could not read");
    println!(input);
}
```

or...

```rust
fn main() {
    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .except("Could not read");
    println!(input);
}
```

## Optimization

### Minimize release size

Why? Just because you can. Small apps that do a lot are fun.

```toml
[profile.release]
opt-level = 'z'     # Optimize for size.
lto = true          # Enable Link Time Optimization
codegen-units = 1   # Reduce number of codegen units to increase optimizations.
panic = 'abort'     # Abort on panic
```

You can reduce the executable from 4.8Mb to 1.2Mb using this configuration in the `Cargo.toml` file.

You can also further reduce the size using the `strip` command to remove symbols.

Source: https://stackoverflow.com/a/54842093

---

[previous](./note-00-new-project-and-failed-compilation-fix.md) |
[next](./note-03-basics.md)
