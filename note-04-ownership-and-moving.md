# Moving, Copying, Cloning, Ownership, Pointers!!!

*honestly this section is one heck of a beast to swallow from the get go*

*trying to keep it down to earth, while still following the book's order, im gonna try to summarize this chapter as best i can.*

## heap and stack

- stack
    - the bits of memory that's declared as the function is executed
    - already defined within the code
    - deterministic as its already embedded in the bytes of the code
- heap
    - bits of memory that's dynamically allocated during the program's executions
    - its size depends on the code and who is executing it
    - non-deterministic as its size is completely dependent on external factors
    - normally slower than memory allocated by the stack

```rs
let a : &str = "i am a string";
let b : String = String::from("i too am a string");
```

an example of the difference here

`a` is of type `&str` because it points to a segment already defined by the code itself.
but it can be used as a normal string.
`b` on the other hand, is also a string, but it's more complex.
behind the scenes, `String` is built on top of a vector, a dynamically sized list.
the value referenced by the variable `a` is on the stack, whereas the value
referenced by `b` is stored within the heap.

## ownership

like the book, it must abide by this rule
(note, it only applies to objects that implement the Drop trait):

- Each value in Rust has a variable that’s called its owner.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped.

(as listed here: https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html)

further consideration for the ownership rules

### Each value in Rust has a variable that’s called its owner.

bound to scope.

ownership can change, but if it does change (think copy/clone), the previous owner
becomes invalid.

primitive values, while they do have ownership,
the do not invalidate the previous owner.

when you let another object borrow a value, you use `&`
example:

```rs
fn main() {
    let a : String::from("hello");
    a.push_str(", world");
    let size = length(&a);
    // ...
}

fn length(s : &String) {
    s.len()
}
```


### There can only be one owner at a time

this applies to mutable reference only.

you can have a bunch of immutable references.

### When the owner goes out of scope, the value will be dropped.

but it also deals with mutability and immutability,
and when it comes to these concepts...
think of intervals.

when you're dealing with a mutable reference, you can modify it within
a specific interval. same goes for immutable.

when you're outside a certain interval,
you cant use values tied to a previous interval.

this works:

```rs

let mut s = String::from("hello");

// interval 1

let r1 = &s;
let r2 = &s;
println!("{} and {}", r1, r2);
// r1 and r2 are no longer used after this point

// interval 2

let r3 = &mut s;
println!("{}", r3);
r3.push_str("z");

// interval 3

let r1 = &s;
let r2 = &s;
println!("{} and {}", r1, r2);
```

why?

because you're not creating variables tied to one interval
derived from the manipulation of the string `s` and using it
outside the interval.

this does not

```rs
// reference function
fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

let mut s = String::from("hello world");

// interval 1

let word = first_word(&s);

// attempting to modify s, note that word is tied to s (points to its values)

s.clear(); // error -> word is used after

// attempting to print the first word (hello) and s, which should be ""

println!("the first word is: {}...{}", word, s);
```

## splice

fetches subsections of iterables and whatnot.

applies to string.

does not have ownership, meaning, it doesn't implement the drop function.

it holds a pointer to the object it's referring to and the size of the pointer itself.

syntax is as followed:

    &variable[init..end]

note that `end` is not inclusive (`init` and `end` can either or both can be omitted)

example:

```rs
let mut string = String::from("hello world");
// points to the first h of the string, and has the size of 5
let hello : &str = &string[..5];

// just remember, with the rules of ownership here
// because hello is being used to print data onto the screen...
// you won't be able to modify the variable string until after the execution of the macro println!

println!("{}", hello);
```

but for whatever reason you wish to hold on to both values, you can do this:


```rs
let mut string = String::from("hello world");
// points to the first h of the string, and has the size of 5
let hello = String::from(&string[..5]);
```

this way, you can use both `string` and `hello` whenever you please.

---

[previous](./note-03-basics.md) |
[next](./note-05-structs.md)
