use std::io::Write;

// use std::vec::Vec;

fn main() {

    fn flush() {
        std::io::stdout().flush().expect("couldn't flush, sorry");
    }

    println!("Begin Here, what do you wish to do?");
    println!(" a - Print the fibonacci to a specific number");
    println!(" b - Convert from celsius to fahrenheit");
    println!(" c - Convert from fahrenheit to celsius");
    println!(" d - Draw a pyramid");
    print!("(a,b,c,d) > ");
    flush();

    let mut user_input = String::new();

    std::io::stdin()
        .read_line(&mut user_input)
        .expect("Could not read your answer");
    
    let mut number = String::new();
    
    println!("Great! Now give me a number!");
    print!("> ");
    flush();
    
    std::io::stdin()
        .read_line(&mut number)
        .expect("Could not read your answer");

    let lower = user_input.to_lowercase();
    let user_input = lower.trim();

    let number = number.trim();

    if user_input == "a" {
        println!("{}", fib(match number.parse() {
            Ok(num) => num,
            Err(_) => 0,
        }));
    }
    else if user_input == "b" {
        println!("{}F", ctof(match number.parse() {
            Ok(num) => num,
            Err(_) => 0.0,
        }));
    }
    else if user_input == "c" {
        println!("{}F", ftoc(match number.parse() {
            Ok(num) => num,
            Err(_) => 0.0,
        }));
    }
    else if user_input == "d" {
        print_pyramid(match number.parse() {
            Ok(num) => num,
            Err(_) => 0,
        });
    }
    else{
        println!("Can't do anything for you here");
    }
}

fn fib(n : u32) -> u32 {
    if n == 0 {
        return 0;
    }
    else if n == 1 {
        return 1;
    }
    return fib(n - 2) + fib(n - 1);
}

fn ctof (c : f64) -> f64 {
    c * 1.8 + 32.0
}

fn ftoc (f : f64) -> f64 {
    (f - 32.0) / 1.8
}

fn print_pyramid(height : u8) {
    
    fn print_seq(time : u8, content : &str) {
        for _ in 0..time {
            print!("{}", content);
        }
    }
    
    let mut start_white = match height.checked_sub(1) {
        Some(num)  => num,
        None => 0 as u8,
    };
    let mut hashes = 1;

    for _ in 0..height {
        print_seq(start_white, " ");
        print_seq(hashes, "#");
        print_seq(start_white, " ");
        print!("\n");

        start_white = match start_white.checked_sub(1) {
            Some(n) => n,
            None => 0 as u8,
        };
        hashes = match hashes.checked_add(2) {
            Some(n) => n,
            None => std::u8::MAX,
        };
    }
}