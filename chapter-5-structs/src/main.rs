
mod user;

// mod user;

fn main() {

    ch51();

    ch43();

    println!("end");

}

fn ch51() {

    {
        struct User {
            email : String,
            username : String,
            sign_in_count : u64,
            active : bool,
        }
        
    
        fn build_user(email: String, username: String) -> User {
            User {
                email,
                username,
                active: true,
                sign_in_count: 1,
            }
        }
    
    
        let mut user1 = User {
            email : String::from("someone@example.com"),
            username : String::from("someusername123"),
            active : true,
            sign_in_count : 1
        };
    
        user1.email.push('z');
    
        let user2 = build_user(String::from("hello"), String::from("pizza"));
    
        let user3 = User {
            email: String::from("another@example.com"),
            username: String::from("anotherusername567"),
            ..user1
        };
    }

    {
        struct Point(i32, i32);

        let pt = Point(0, 3);
        let pt2 = Point(4, 0);

        fn abs(i : i32) -> i32 {
            i32::abs(i)
        }

        fn base(pt : &Point, pt2 : &Point) -> i32 {
            abs(pt.0 - pt2.0)
        }

        fn side(pt : &Point, pt2 : &Point) -> i32 {
            abs(pt.1 - pt2.1)
        }

        fn diagonal(pt : &Point, pt2 : &Point) -> f64{
            let b = base(pt, pt2);
            let s = side(pt, pt2);
            ((b * b + s * s) as f64).sqrt()
        }

        fn precision(value : f64, comparison : f64, precision : f64) -> bool {
            if value == comparison {
                return true;
            }
            (
                f64::max(value, comparison) - f64::min(comparison, precision)
            ) <= precision
        }

        println!("diagonal is {}", diagonal(&pt, &pt2));
        println!("{} answer is within precision", precision(diagonal(&pt, &pt2), 5.0, 0.0000001));
    }

    {
        #[derive(Debug)]
        struct Ayy();
        let ayy = Ayy();
        println!("{:#?} has nothing", ayy);
        let st = ();
        let s2 = ();
        println!("{}", st == s2);
    }
}


#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
    
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn ch43() {
    {
        let rec = Rectangle {
            width : 10,
            height : 5
        };

        println!("the area of the triangle is {}", rec.area());
    }

    {
        let rect1 = Rectangle {
            width: 30,
            height: 50,
        };
        let rect2 = Rectangle {
            width: 10,
            height: 40,
        };
        let rect3 = Rectangle {
            width: 60,
            height: 45,
        };

    
        println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
        println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));
    }

    {
        let a = Rectangle::square(10);
        println!("the area of the triangle is {}", a.area());
    }
}