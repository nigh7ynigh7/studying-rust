# project structure

when you run 

    cargo new project

the project you create is, in fact, a crate

crates can

- be a library (by having `src/lib.rs`)
- or be a binary (by having `src/main.rs`)
- or be both

## creating a lib

it should be straightforward enough.

you can start by doing this:

    cargo new --lib my-lib

and rust will make a project with the following structure:

    /my-lib
        + /src
            + lib.rs
        + Cargo.toml

Apparently, by default, `lib.rs` will contain a test

## project, crates, modules, etc

- a package is what you get when you execute `cargo new ...`
    - it has a `Cargo.toml` file and a `src/main.rs` or `src/lib.rs` file.
    - by rule, a package can only have up to 1 `src/lib.rs` crate;
    - and as many binary crates as you want.
- a crate, on the other hand, is either a lib or a binary.
    - it's a lib if you have `src/lib.rs` file
    - it's a binary if you have a `src/main.rs` with `fn main {}` function
        - the additional binaries files are placed in `src/bin` directory
    - by definition a _crate is an entrypoint into the inner modules/functions the package offers`.
- a `module` is a group of code
    - by definition it's _a collection of items_
    - a crate has modules, code, etx
    - but they can contain anything
        - enums, sturcts, functions, etc.


## paths

it's basically a way of referring to code within a module/file/crate.

- absolute
    - `crate`
        - `crate::module`
        - uses the root of the current crate as reference
    - `{package-name}`
        - `page_name::module`
        - imports from other crates in the project (i.e. `src/lib.rs`)
- relative
    - `self`
        - refers to current object
        - allows you to spread a module out for usage
    - `super`
        - refers to parent obj

# other curiosities

without the `pub` keyword, you can't use things from within the module. rust has its own mechanism of encapsulation/accessibility.

use the `#[path = "path/to/mod.rs"] mod any_name`
to import a module in a sub directory

use `pub use module::...` to import and publish whatever is 
in the referred module.

```rs
use std::fmt::Result;
use std::io::Result as IoResult;

fn function1() -> Result {
    // --snip--
}

fn function2() -> IoResult<()> {
    // --snip--
}```