
// use std::mem::drop;

fn main() {
    println!("Chapter 4");
    println!("Ownership");
    println!("\n");
    c41();

    println!("\n");
    c42();

    println!("\n");
    c43();
}

fn c41() {
    println!("4.1 ownership and scope\n");
    fn scope() {
        println!("4.1.1 scope\n");

        let var = "hello";
        
        println!("the var {} will not leave this scope", var);

        println!("as this function reaches its end, the var variable will reach its end");
        println!("meaning, it will go out of scope");
        println!("\n");
    }
    scope();

    fn string() {
        println!("4.1.2 the string type\n");
        let mut s = String::from("hello");
        s.push_str(", world!");
        println!("{}", s);
        println!("string is a complex object, not a simple stackable object");
        println!("str x string");
        println!("the string object has a .drop() thats executed once it goes out of scope");
        println!("which is handled by ");
        // //  manually drops the variable
        // drop(s);
        // println!("{}", s);
    }
    string();

    fn move_and_copy () {
        println!("4.1.3 moving and copying\n");
        let x = 5;
        let y = x;
        println!("this works, {} {}", x, y);

        let s1 = String::from("hello");
        let s2 = s1;
        // println!("this does not work, {} {}", x, y);
        println!("in this case, only s2({}) is usable", s2);
        println!("rust invalidated s1 as soon as it's sent to s2");

        println!("\nmeaning, for primitives, rust copies, for complex objs");
        println!("rust moves, invalidating the previous reference");
        println!("if you want to access copy the data, use the clone() method");
        let s1 = String::from("hello");
        let mut s2 = s1.clone();
        println!("s1 = {}, s2 = {}", s1, s2);
        s2.push_str(" and beware!");
        println!("but they do diverge afterwards, so be careful here.");
        println!("{}", s2);

        println!("\nbut remeber, stack only data is copied outright");
        let a = "hawat";
        let b = a;
        println!("a = {}, b = {}", a, b);
        println!("remember, this is a &str type. a reference to a string, which is copied");

        // // since this isn't complex, cloning here does nothing
        let x = 5;
        let y = x.clone();
        let y = y;
        println!("x = {}, y = {}", x, y);

        println!("you can implement a trait called Copy onto a complex object");
        println!("allowing it to act as if it were on a stack.");
        println!("rule of thumb, however, if the object has the Drop trait implemented, it cannot implement the Copy trait.");
        println!("but more on that later....");
    }
    move_and_copy();

    fn ownership() {
        println!("4.1.4 ownership\n");
     
        println!("consider the var x");
        let x = 31;
        
        fn this_copies(x: i32) {
            println!("{}", x);
        }
        this_copies(x);
        println!("x after calling this_copies {}", x);

        let s1 = String::from("ara ara");

        fn this_takes_ownership(a : String) {
            println!("{}", a);
            println!("meaning that the input string will be dropped by me");
            println!("and not the one who called me");
        }
        this_takes_ownership(s1);
        println!("the string s1 is no longer accessible after calling this_takes_ownership");

        
        fn this_gives_back_ownership(a : String) -> String {
            println!("{}", a);
            println!("meaning that the input string will be dropped by me");
            println!("and not the one who called me");
            println!("but i can return it...");
            a
        }
        let s1 = String::from("ara ara");
        let s2 = this_gives_back_ownership(s1);
        println!("{}", s2);
        println!("wow! giving back to those who give things to you!");
        println!("but note, since it was returned to s2 and not s1, s1 remains invalid");
    }
    ownership();
}

fn c42() {
    println!("4.1 references and borrowing \n");

    println!("& is referencing and * is dereferencing");
    println!("& -> implies the address location of an object");
    println!("* -> gives back the object of the referenced object");


    fn ownership() {
        println!("4.1.1 borrowing\n");
        
        fn calculate_length(s: &String) -> usize {
            s.len()
        }

        let s1 = String::from("hello");
        let len = calculate_length(&s1);
    
        println!("The length of '{}' is {}.", s1, len);

        println!("");
        println!("observe how s1 is still valid");
        println!("what you did here is:");
        println!("you sent a reference of the string object");
        println!("that the function calculate_length then used to process its contents");
        println!("and it did free s as well, but not the main obj, but its reference");

        // fn another(s: &String) -> String {
        //     let a = *s;
        //     return a.clone()
        // }
        // let b = String::from("ayy");
        // another(&b);
        // println!("{}", b);

        println!("and lol, to dereference something you need to implement the copy trait");
    }


    ownership();


    fn mutable_ownership() {
        // fn change(some_string: &String) {
        //     some_string.push_str(", world");
        // }
        
        // let s = String::from("hello");
        // change(&s);

        println!("the above does not work, why? because it's immutable");
        println!("the immutability depends on the variable's own declaration");
        println!("if it's immutable, then so will the reference accessability");
        println!("if it isn't you'll be able to alter it");

        fn change2(some_str : &mut String) {
            some_str.push_str(", world");
        }

        let mut s = String::from("hello");
        change2(&mut s);
        println!("{}", s);

        println!("the above worked because the base variable is mutable");
        println!("and you sent a mutable reference");
        println!("the transition from variable mutable to immutable reference works");
        println!("but the transition from immutable to mutable does not work");

        
        fn readme(some_string: &String) {
            println!("{}", some_string);
        }
        
        readme(&s);

        println!("case in point, see above");
    }
    mutable_ownership();

    fn borrowing_and_scope() {
        let mut s = String::from("hello");

        let r1 = &mut s;
        // let r2 = &mut s;
    
        println!("{}", r1/* , r2 */);
        
        println!("be weary here. you can only have one mutable reference active at a time");
        
        fn readme(some_string: &String) {
            println!("- {}", some_string);
        }

        readme(&mut s);

        println!("but don't get ahead of yourself here.");
        println!("this means that you can't have 2 of the same reference *in the same scope*");
        println!("you can, however, pass on mutable reference in function calls");

        {
            let mut s = String::from("hello");

            let r1 = &s; // no problem
            let r2 = &s; // no problem
            println!("{} and {}", r1, r2);
            // r1 and r2 are no longer used after this point
        
            let r3 = &mut s; // no problem
            println!("{}", r3);
            r3.push_str("z");

            let r1 = &s; // no problem
            let r2 = &s; // no problem
            println!("{} and {}", r1, r2);

            println!("also, t his is allowed because: the rule is for **mutable references**");
        }

        println!("a curious element to the previous block of code is:");
        println!("- you can have multiple immutable references");
        println!("- but when you transition the reference back to mutable, the previous immutable references become invalid");
        println!("- but you can redeclare them after modifying them");
    }

    borrowing_and_scope();

    fn dangling_pointers() {
        // fn bad_fun() -> &String {
        //     let a = String::from("pizza");
        //     &a
        // }
        println!("the above will return a pointer with a non existent target");
        println!("solution:");
        
        fn good_fun()  -> String {
            let a = String::from("pizza");
            a
        }
        println!("{}", good_fun());

        println!("why does it work?");
        println!("- think back to the whole ownership dilemma.");
        println!("- you create and instance and give the calling scope the ownership");

        println!("why does the first one not work?");
        println!("- because , while you might return a reference");
        println!("- as soon as the end of its scope is reached, the declared value is deallocated");
        println!("- rendering the pointer moot");
    }

    dangling_pointers();
}

fn c43() {
    // let s1 = String::from("hello world");
    // let s2 = &s1[0..5];

    println!("the problem proposed by the book is simple");
    println!("to return the first word of any european language");
    println!("to do this, you must calculate where the space is... return a subsection of the string");

    { // proposed solution
        println!("this is the proposed solution");
        fn first_word(s: &String) -> usize {
            let bytes = s.as_bytes();
        
            for (i, &item) in bytes.iter().enumerate() {
                if item == b' ' {
                    return i;
                }
            }
        
            s.len()
        }

        let mut s = String::from("hello world");
        let word = first_word(&s); // word will get the value
        println!("before clear {}, {}", word, s);
        s.clear();
        println!("after clear {}, {}", word, s);

        println!("i got the index i needed...");
        println!("but the problem still stands, i need that string val");
    }

    {
        println!("introducing slices...");
        println!("an example of it in action {}", &("hello world")[0..5]);
        println!("[init...end], [init..] [..end] or [..]");
        println!("they dont have ownership");
        println!("and they're pretty much borrowed data...");

        {
            println!("example below");
            let s = String::from("hello world");
            let hello = &s[0..5];
            // let world = &s[6..11];
            println!("{}", hello);
        }
    }

    {
        println!("so... back to solving the problem with slices...");

        fn first_word(s: &str) -> &str {
            let bytes = s.as_bytes();
        
            for (i, &item) in bytes.iter().enumerate() {
                if item == b' ' {
                    return &s[0..i];
                }
            }
        
            &s[..]
        }

        let mut s = String::from("hello world");
        let word = first_word(&s);
        // s.clear(); // error -> word is used after
        println!("the first word is: {}...{}", word, s);
        s.clear(); // the clear works here, though
    }

    {
        println!("please not that slices also apply to iterables such as arrays, tuples, vectors, etc.");
    }

    {
        let mut string = String::from("hello world");
        // points to the first h of the string, and has the size of 5
        let hello : &str = &string[..5];

        // just remember, with the rules of ownership here
        // because hello is being used afterwards here,
        // you won't be able to modify the variable string until after the execution of the macro println!

        println!("{}", hello);

        string.push_str("yup");
    }
}