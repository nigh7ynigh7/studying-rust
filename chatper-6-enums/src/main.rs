fn main() {
    println!("Hello, world!");
    ch61();
    ch62_match();
    ch63_if_let();
}

fn ch61() {
    fn c61_a() {
        enum IpAddrKind {
            V4,
            V6,
        }
        
        struct IpAddr {
            kind : IpAddrKind,
            address : String,
        }


        let four = IpAddrKind::V4;
        let six = IpAddrKind::V6;

        fn route(ip_kind : IpAddrKind) { }
        route(IpAddrKind::V4);
        route(IpAddrKind::V6);

        let home = IpAddr {
            kind : IpAddrKind::V4,
            address : String::from("127.0.0.1"),
        };

        let loopback = IpAddr {
            kind : IpAddrKind::V6,
            address : String::from("::1"),
        };

        // here we have a basic enum being used as a child member.
        // easy enough to deal with.
    }

    fn ch61_b() {
        enum IpAddr {
            V4(String),
            V6(String),
        }

        let home = IpAddr::V4(String::from("127.0.0.1"));
        let loopback = IpAddr::V6(String::from("::1"));

        fn print(addr : &IpAddr) {
            match addr {
                IpAddr::V4(val) => println!("{}", val),
                IpAddr::V6(val) => println!("{}", val),
            }
        }

        print(&home);
        print(&loopback);

        // here we learn that we can couple data into enums
        // this makes them more dynamic
        // java doesn't really have something like this
        /*
        * while you can do this:
        *
        * public enum MyEnum {
            A("a"), B("n");
            MyNum(str : String) {
            // any
            }
        }
        * you're not really coupling any contextual data with it.
        * you're just denoting something else
        */

        println!("another thing to note, when you're identifying data");
        println!("use enums pls");
    }

    fn ch61_c() {
        enum IpAddr {
            V4(u8, u8, u8, u8),
            V6(String),
        }

        let home = IpAddr::V4(127, 0, 0, 1);
        let loopback = IpAddr::V6(String::from("::1"));

        fn print(addr : &IpAddr) {
            match addr {
                IpAddr::V4(a,b,c,d) => println!("{}.{}.{}.{}", a, b, c, d),
                IpAddr::V6(val) => println!("{}", val),
            }
        }

        print(&home);
        print(&loopback);

        println!("you're not limited to ONLY one input");
    }

    fn ch61_d() {
        struct Ipv4Addr {
            // --snip--
        }
        
        struct Ipv6Addr {
            // --snip--
        }
        
        enum IpAddr {
            V4(Ipv4Addr),
            V6(Ipv6Addr),
        }

        println!("anything goes!");
    }

    fn ch61_e() {
        enum Message {
            Quit,
            Move { x : i32, y : i32 },
            Write (String),
            ChangeColor (i32, i32, i32),
        }

        impl Message {
            fn call(&self) {
                match &self {
                    Message::Write(val) => println!("{}", val),
                    _ => println!("not supported, sorry")
                }
                
                println!("do things with message here");
            }
        }

        let m = Message::Move {
            x : 19,
            y : 1000,
        };

        m.call();

        let w = Message::Write(String::from("limão... yeah"));

        w.call();
    }


    fn ch61_e_alt() {

        struct QuitMessage; // unit struct
        struct MoveMessage {
            x: i32,
            y: i32,
        }
        struct WriteMessage(String); // tuple struct
        struct ChangeColorMessage(i32, i32, i32); // tuple struct

        enum Message {
            Quit (QuitMessage),
            Move (MoveMessage),
            Write (WriteMessage),
            ChangeColor (ChangeColorMessage),
        }

        let m = Message::Move( MoveMessage {
            x : 0,
            y : 10,
        });

        // up to implementation here
    }

    fn ch61_f_options() {
        fn optional(val : u64) -> Option<u64>{
            if val % 2 == 0 {
                Some(val)
            } else {
                None
            }
        }
        let value = optional(122);

        println!("if the value divisible by 2? {}", match value {
            Some(_) => "yeh",
            _ => "nah",
        });
    }

    c61_a();
    ch61_b();
    ch61_c();
    ch61_e();
    ch61_e_alt();
    ch61_f_options();
}

fn ch62_match() {
    // // going out on a tangent here, testing functions as a reference
    // // its doable
    // fn test(hello : &(Fn(i32) -> i32)) -> i32 {
    //     hello(10)
    // }

    fn demoing_match() {
        enum Coin {
            Penny, Nickle, Dime, Quarter
        }

        fn value_in_cents(coin : Coin) -> u8 {
            match coin {
                Coin::Penny => {
                    println!("lucky????");
                    1
                },
                Coin::Nickle => 5,
                Coin::Dime => 10,
                Coin::Quarter => 25,
            }
        }

        println!("value for the Penny enum is {}", value_in_cents(Coin::Penny));
    }

    fn binding_values() {
        #[derive(Debug)] // print stuff without implementing things
        enum UsState {
            Alabama,
            Alaska,
            // --snip--
        }

        enum Coin {
            Penny,
            Nickel,
            Dime,
            Quarter(UsState),
        }

        fn value_in_cents(coin: Coin) -> u8 {
            match coin {
                Coin::Penny => 1,
                Coin::Nickel => 5,
                Coin::Dime => 10,
                Coin::Quarter(state) => {
                    println!("State quarter from {:?}!", state);
                    25
                }
            }
        }

        println!("the value of this coin is: {:?}", value_in_cents(
            Coin::Quarter(UsState::Alaska)
        ));
    }

    demoing_match();
    binding_values();

    fn plus_one(x: Option<i32>) -> Option<i32> {
        match x {
            None => None,
            Some(i) => Some(i + 1),
        }
    }

    
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);

    println!("inb4, there are examples of match being applied to options");

    println!("btw, match must return a result");
    // // the following will fail, without doubt
    // fn plus_one(x: Option<i32>) -> Option<i32> {
    //     match x {
    //         Some(i) => Some(i + 1),
    //     }
    // }

    println!("_ is a wildcard with these fellers");

    println!("so, you can have a match like: {}", match 10 {
        10 => 100,
        _ => 0,
    });
}

fn ch63_if_let() {

    println!("think of the following piece of code");
    {
        let some_val = Some(8_888);
        match some_val {
            Some(3) => println!("yeh"),
            _ => (),
        }
    }

    println!("this is equivalent to the previous code");
    {
        let some_val = Some(8_888);
        if let Some(3) = some_val {
            println!("yeh");
        }
    }

    println!("why use it?");
    println!("because you have the specific advantage of:");
    println!("not needing to write an exhaustive match for one value only");

    println!("sugar syntax");

    println!("final ex for this chap");

    {
        println!("btw, you can tag an else there to that if let");
        println!("rebuilding the previous model");

        
        #[derive(Debug)] // print stuff without implementing things
        enum UsState {
            Alabama,
            Alaska,
            // --snip--
        }

        enum Coin {
            Penny,
            Nickel,
            Dime,
            Quarter(UsState),
        }

        {
            println!("before...");
            let mut count = 0;
            match Coin::Quarter(UsState::Alaska) {
                Coin::Quarter(state) => println!("State quarter from {:?}!", state),
                _ => count += 1,
            }
            println!("{}", count);
        }

        {
            println!("after...");
            let mut count = 0;
            if let Coin::Quarter(UsState::Alaska) = Coin::Quarter(UsState::Alaska) {
                println!("State quarter from {:?}!", UsState::Alaska);
            } else {
                count += 1;
            }
            println!("{}", count);
        }


    }

}