## Takeaways

- The loop construct is very similar to python (whiles + fors)
- There is an indefinite loop function for rust

```rust
loop {
    println!("weeeee!!! never gonna give you up");
}
```

- Pretty much anything within `{}` is a block of code

```rust
let a = {
    let b = 10;
    b * b
}

let b = if a == 100 { "10 sq" } else { "meh" }

{
    // doing something here....
    for i in 0..=10 {
        println!("{}", {
            prinln("doubling {}", i);
            i * i
        });
    }
}
```

- you specify a return type with `->`

```rust
fn hello_world() -> i32 {
    54
}
```

- tuples exist

```rust
// you can just throw them in
let a = (1, 'y', 2.0);

// you can explode them like python
let (a,b,c) = (1,'y',2.0);

// you can specify the type
let a : (i32, &str, bool) = (1414, "hell yeah", {
    println!("just for the sake of it");
    false
});
```

- you cannot attribute multiple times in the same statement (`a = b = c`)
- regarding variables
    - you can shadow `let` variables
    - but you cannot shadow `const`
    - constants should be uppercased
    - `let` variables cannot be edited
        - to allow them to be editable, use `let mut`
        - `mut` for mutable, or changeable

- like python, `()` parenthesis aren't required everywhere

## Functions

- you can declare functions within functions and use them within their parent scope
    - the order doesn't really matter,
        - you use the function before the code reaches the end

## Sidenotes

- `&` means you're sending a pointer to the function value, the reference value

```rust
println!("the pointer for this number 5: {p}", &5);
```

- use `_` within numbers to visually separate them: `let i = 10_000`

---

[previous](./note-01-match-result-cargo-rustc-optimization.md) |
[next](./note-04-ownership-and-moving.md)