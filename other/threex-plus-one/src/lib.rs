
pub struct ThreeTimesOne {
    pub start : u64,
    pub current : u64,
    pub max : u64,
    pub end : bool,
    pub steps : Vec<u64>
}

impl ThreeTimesOne {
    pub fn new(input : u64) -> ThreeTimesOne {
        return ThreeTimesOne {
            current : input,
            start : input,
            max : input,
            end : input == 0 || input == 1,
            steps : Vec::new()
        }
    }

    pub fn next(&mut self) -> u64 {
        let nx = if &self.current % 2 == 0 {
            self.current / 2
        } else {
            let chosen = self.current * 3 + 1;
            if chosen > self.max {
                self.max = chosen;
            }
            chosen
        };

        self.steps.push(nx);
        self.current = nx;

        if nx == 1 {
            self.end = true
        }

        return nx;
    }

    pub fn count_steps(&self) -> usize {
        self.steps.len()
    }

    pub fn finalize(&mut self) -> usize {
        while !self.end {
            self.next();
        }
        self.count_steps()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn one_is_end(){
        assert!(ThreeTimesOne::new(1).end);
    }
}