use threex_plus_one::ThreeTimesOne;

fn main() {
    let mut a = ThreeTimesOne::new(27);
    println!("steps: {}", a.finalize());
    println!("start: {}", a.start);
    println!("max  : {}", a.max);
}
