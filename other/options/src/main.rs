fn main() {
    println!("Hello, world!");
    println!("for 0 {}", match zero_or_null(0) {
        Some(_) => "yeah",
        None => "none",
    });
    println!("for 1 {}", match zero_or_null(1) {
        Some(_) => "yeah",
        None => "none",
    });
}


fn zero_or_null(input : i32) -> Option<i32> {
    if input == 0 {
        Some(0)
    }
    else {
        None
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn zero_is_zero() {
        assert_eq!(match zero_or_null(0) {
            Some(x) => if x== 0 { true } else { false },
            None => false,
        } , true);
    }
}