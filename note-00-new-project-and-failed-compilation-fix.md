# Creating a new project

To create a new project, just execute the following command

    $ cargo new project_name

You'll have a new hello world project

To see it running, `cd` into the project
and execute the following:

    $ cargo run

## Issues that appeared

(At the time, i was using Windows)

Since I didn't want to install Visual Studio and all of the heavy baggage that comes with it, i couldn't use `cargo run` or `cargo build` because the program `link.exe`, causing the build to fail.

A solution I found: https://stackoverflow.com/a/62817909

All i had to do was the following:

    $ rustup toolchain install stable-x86_64-pc-windows-gnu
    $ rustup default stable-x86_64-pc-windows-gnu

And it worked.

And it probably worked because somewhere down the line, i downloaded and installed mingw.

---

[next](./note-01-match-result-cargo-rustc-optimization.md)