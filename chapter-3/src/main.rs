use std::cmp::Ordering;

fn main() {
    println!("Hello, world!");
    println!("Chapter 3.1 - variables, mutability, constants");
    chapter31();
    println!("\nChapter 3.2 - data types, tuples, lists");
    chapter32();
    println!("\nChapter 3.3 - functions");
    chapter33();
    
    println!("\nChapter 3.4 - comments");
    println!("Chapter 3.4 - //");
    println!("Chapter 3.4 - /* ... */");

    
    println!("\nChapter 3.3 - functions");
    chapter35();
}

fn chapter31() {
    // // failed case
    // let x = 10;
    // println!("the value oc x is {}", x);
    // x = 5;
    // println!("the value oc x is {}", x);

    
    // Solution to above
    let mut x = 10;
    println!("the value oc x is {}", x);
    x = 5;
    println!("the value oc x is {}", x);

    const MAX_PTS : u32 = 100_000;
    println!("max points are {}", MAX_PTS);

    // shadowing
    let x = 5;
    let x = x + 1;
    let x = x + 2;
    println!("the value of x is {}", x);

    // keep in mind though, different addresses

    let y = 0;
    println!("address of this y is {:p}, value {}", &y, y);
    let y = 10;
    println!("address of this y is {:p}, value {}", &y, y);

    println!("However, after setting y to mut, the address remains the same:");
    
    let mut y = 0;
    println!("address of this y is {:p}, value {}", &y, y);
    y = 10;
    println!("address of this y is {:p}, value {}", &y, y);
}


fn chapter32() {
    let y : i32 = -1_223;
    println!("value of y {}", y);
    let y : u8 = 0b11111111;
    println!("value of y {}", y);
    let y : u16 = 0xFFFF;
    println!("value of y {}", y);
    let y : u8 = b'A';
    println!("value of y {}", y);

    let mut a : u8 = 0xFF;
    a = match a.checked_add(10) {
        Some(v) => v,
        None => {
            println!("Overflow here");
            0
        }
    };

    println!("value of a {}", a);

    let tup : (String, i32, f64) = (String::from("hello"), -17, 32.98);
    println!("value of 0 of tuple: {}", tup.0);
    println!("value of 2 of tuple: {}", tup.2);
    let ( a, b, c ) = tup;
    println!("values of the tuple: {} {} {}", a, b, c);

    // allocated on the stack
    let arr = [1,2,3,4,];
    println!("values of the array: {}", arr[0]);

    let months = ["January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"];

    for m in months.iter() {
        println!("{}", m);
    }

    let fixed_arrr : [i32; 5] = [1,2,3,4,5,];
    println!("values of the array: {}", fixed_arrr[0]);
}

fn chapter33() {
    fn test() {
        println!("function in another function")
    }

    test();
    // so wow, we can make functions in functions
    // so wow, we can call them in them too

    another_function(8, 10);

    fn another_function(x : i32, y : i32) {
        println!("The value of x is: {}", x);
        println!("The value of y is: {}", y);
    }
    // wow, and we can do so in no specific order

    another_function(5, 6);

    // inner scope, amazing, incredible! wonderful!
    {
        let a = 10;
        println!("{}", a);
    }

    let z = {
        let z = 100;
        z * z
    };
    println!("{}", z);

    // // apparently, no multiapplying;
    // let mut a : i32 = 10;
    // let mut b : i32 = 11;
    // a = b = 100;

    println!("{}", {
        println!("in the scope");
        1337
    });

    let mut z = 10;
    let y = {
        let output = 11;
        z = z + output;
        {
            output
        }
    };

    println!("{} {}", z, y);


    fn five () -> i32 { 5 };

    let x = five();
    println!("The value of x is: {}", x);

    fn plus_one(x: i32) -> i32 {
        x + 1
    }

    let x = plus_one(5);
    println!("The value of x is: {}", x);
}



fn chapter35() {

    let mut a = 0;
    loop {
        a += 1;
        println!("on going {}", a);
        if a == 10 {
            println!("done");
            break;
        }
    }

    for i in 5..10 {
        println!("{}", i);
    }

    match a.cmp(&(5)) {
        Ordering::Equal => {
            println!("Ordering::Equal");
        },
        Ordering::Greater => {
            println!("Ordering::Greater");
        }
        Ordering::Less => {
            println!("Ordering::Less");
        }
    }

    let za = 6;

    if za % 6 == 0 {
        println!("za is divisble by six");
    }
    else if za % 4 == 0 {
        println!("za is divisble by four");
    }
    else if za % 2 == 0 {
        println!("za is divisble by twa");
    }
    else {
        println!("za is i give by twa");
    }

    let za2 = if true { 3 } else { 5 };
    println!("divvy {}", za2);
    let za2 = if false { 3 } else { 5 };
    println!("divvy {}", za2);

    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");

    
    for number in (1..za2).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");

    // let a = 1..10;
    
    // let ya = [1,2,3,4,5];
    // ya[za2]
}